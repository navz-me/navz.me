<?php

session_start();
require_once( dirname(__FILE__) . '/vendor/phpmailer/PHPMailerAutoload.php');

function html_contact_body($name, $email, $phone, $message){
return <<<EOT
<table width="671" border="0" align="center" cellpadding="3" cellspacing="3">
  <tbody>
    <tr>
      <td colspan="2" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" align="center">The following information was submitted from <br>
      <a href="https://www.navz.me/">https://www.navz.me/</a> website.</td>
    </tr>
    <tr>
      <td align="right">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="130" align="right" valign="top"><strong>Name:  &nbsp;</strong></td>
      <td width="520" valign="top"><div>
        <div>$name</div>
      </div></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>Email Address:&nbsp; </strong></td>
      <td valign="top"><div>
        <div>$email</div>
      </div></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>Phone: &nbsp;</strong></td>
      <td valign="top"><div>
        <div>$phone</div>
      </div></td>
    </tr>
    <tr>
      <td align="right" valign="top"><strong>Message:&nbsp; </strong></td>
      <td valign="top"><div>
        <div>$message</div>
      </div></td>
    </tr>
  </tbody>
</table>
EOT;
}

if( !empty($_SESSION['is_visitor']) and !empty($_POST['name']) and !empty($_POST['email']) and !empty($_POST['phone']) and !empty($_POST['message']) ){
    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $phone = trim($_POST['phone']);
    $message = trim($_POST['message']);

    if( empty($name) and empty($email) and empty($phone) and empty($message) ){
        die('0 | All the fields are required.');
    } else if( !preg_match("/^[a-zA-Z0-9-' ]+$/", $name) ){
        die('0 | The name field contains invalid characters.');
    } else if( strlen($name) < 2 ){
        die('0 | The name field can have minimum of 2 characters.');
    } else if( strlen($name) > 55 ){
        die('0 | The name field can have maximum of 55 characters.');
    } else if( !filter_var($email, FILTER_VALIDATE_EMAIL) ){
        die('0 | The email field contains invalid email format.');
    } else if( strlen($email) < 5 ){
        die('0 | The email field can have minimum of 5 characters.');
    } else if( strlen($email) > 55 ){
        die('0 | The email field can have maximum of 55 characters.');
    } else if( !preg_match("/^[0-9-+() ]+$/", $phone) ){
        die('0 | The phone field contains invalid phone format.');
    } else if( strlen($phone) < 7 ){
        die('0 | The phone field can have minimum of 7 digits.');
    } else if( strlen($phone) > 55 ){
        die('0 | The phone field can have maximum of 17 digits.');
    } else if( strlen($message) < 5 ){
        die('0 | The message field can have minimum of 5 characters.');
    } else if( strlen($message) < 5 ){
        die('0 | The message field can have maximum of 255 characters.');
    } else if( preg_match("/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/", $message) ){
        die('0 | The message field cannot have any links.');
    } else {
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->IsHTML(true);
        $mail->Host = "smtp.mailgun.org";
        $mail->Port = 587;
        $mail->SMTPAuth = true;
        $mail->Username = "postmaster@navz.me";
        $mail->Password = "%7HzW7Scj1&z";
        $mail->setFrom($email, $name);
        $mail->addAddress('navneil.naicker@gmail.com');
        $mail->Subject = 'Website Contact - ' . $name . ' has left a message.';
        $mail->Body = html_contact_body($name, $email, $phone, $message);
        if( $mail->send() ){
            die('1 | Hi ' . $name . '. Thank you so much for reaching out to me. I will reply to you as soon as I see your email on my inbox.');
        } else {
            echo "Mailer Error: " . $mail->ErrorInfo;
        }
    }
} else {
    die('0 | All the fields are required.');
}