<?php

    function sanitize_output($buffer) {
        $search = array('/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s');
        $replace = array('>', '<', '\1');
        $buffer = preg_replace($search, $replace, $buffer);
        return $buffer;
    }
    ob_start("sanitize_output");

    session_start();    
    $_SESSION['is_visitor'] = true;

    $page_title = "Navneil Naicker – Full Stack Web and Mobile Developer";
    $page_description = "A self-taught full-stack developer from Suva City, Fiji passion and has vast knowledge with experiences in various technologies.";
    $page_image = "//www.navz.me/images/navneil.naicker-full-stack-developer.png";
    $page_twitter_image = "//www.navz.me/images/twitter-card.jpg";
    $page_url = "//www.navz.me/";
?>
<!DOCTYPE html>
    <!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $page_title; ?></title>
        <meta name="description" content="<?php echo $page_description; ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" sizes="57x57" href="./images/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="./images/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="./images/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="./images/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="./images/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="./images/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="./images/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="./images/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="./images/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="./images/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="./images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="./images/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon/favicon-16x16.png">
        <link rel="manifest" href="./images/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#424242">
        <meta name="msapplication-TileImage" content="./images/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#424242">
        <meta name="description" content="<?php echo $page_description; ?>"/>
        <meta name="application-name" content="Navneil Naicker"/>
        <meta property="og:title" content="<?php echo $page_title; ?>"/>
        <meta property="og:type" content="article"/>
        <meta property="og:image" content="<?php echo $page_image; ?>"/>
        <meta property="og:description" content="<?php echo $page_description; ?>"/>
        <meta name="twitter:card" content="summary"/>
        <meta name="twitter:creator" content="@navzme">
        <meta name="twitter:site" content="@navzme">
        <meta name="twitter:title" content="<?php echo $page_title; ?>"/>
        <meta name="twitter:description" content="<?php echo $page_description; ?>"/>
        <meta name="twitter:image" content="<?php echo $page_image; ?>"/>
        <?php if($_SERVER['REMOTE_ADDR'] != '127.0.0.1' ){ ?> 
        <script defer>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-66085363-1', 'auto');
            ga('send', 'pageview');
            var _gaq = _gaq || [];
            setTimeout(function(){ _gaq.push(['_trackEvent', 'Control', 'Bounce Rate', ''])}, 300 );
        </script>
        <?php } ?>
    </head>
    <body style="display: none;">
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div id="hero-card"><?php require_once( dirname(__FILE__) . '/modules/hero-card.php' ); ?></div>
        <div id="primary-nav"><?php require_once( dirname(__FILE__) . '/modules/primary-nav.php' ); ?></div>
        <div id="about-me"><?php require_once( dirname(__FILE__) . '/modules/about-me.php' ); ?></div>
        <div id="my-resume"><?php require_once( dirname(__FILE__) . '/modules/my-resume.php' ); ?></div>
        <div id="my-weapons"><?php require_once( dirname(__FILE__) . '/modules/my-weapons.php' ); ?></div>
        <div id="see-my-portfolio"><?php require_once( dirname(__FILE__) . '/modules/see-my-portfolio.php' ); ?></div>
        <div id="shoot-a-message"><?php require_once( dirname(__FILE__) . '/modules/shoot-a-message.php' ); ?></div>
        <script type="text/javascript" src="https://www.filehosting.org/file/details/805015/bundle.min.js"></script>
        <script type="text/javascript">
            var bundleScript = document.createElement('link');
            bundleScript.rel = 'stylesheet';
            bundleScript.href = './css/bundle.min.css';
            bundleScript.type = 'text/css';
            var bundleScriptDefer = document.getElementsByTagName('link')[0];
            bundleScriptDefer.parentNode.insertBefore(bundleScript, bundleScriptDefer);

            var fontStyle = document.createElement('link');
            fontStyle.rel = 'stylesheet';
            fontStyle.href = 'https://fonts.googleapis.com/css?family=Open+Sans|Orbitron';
            fontStyle.type = 'text/css';
            var fontStyleDefer = document.getElementsByTagName('link')[0];
            fontStyleDefer.parentNode.insertBefore(fontStyle, fontStyleDefer);
        </script>
    </body>
</html>