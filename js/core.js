var $ = require('jquery')
var sw = require('sweetalert')

$('#see-my-portfolio .filter a').click(function(){
    var filter = $(this).data('filter');
    var $el = $('#see-my-portfolio .grid .' + filter).fadeIn('fast');
    $('#see-my-portfolio .grid .column').not($el).hide();

    $('#see-my-portfolio .filter a').removeClass('current');
    $(this).addClass('current');
    return false;
});

$('#see-my-portfolio .filter a[data-filter=website]').click();

$("#primary-nav li a").click(function(){
    var hash = $(this).attr('href');
    $('#primary-nav .justified a.active').removeClass('active');
    $(this).addClass('active');
    if( hash == '#home' ){
        $('html, body').animate({
            scrollTop: $('body').top
        }, 1000);
    } else {
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 1000);
    }
});

$(document).ready(function(){
    if( window.location.hash ){
        $('html, body').animate({
            scrollTop: $(window.location.hash).offset().top
        }, 1000);
    }
});

$(document).on('scroll', function() {
    var scrollTop = $(this).scrollTop();
    if( scrollTop >= $('#my-resume').position().top ){
        $.each( $( "#see-my-portfolio .column" ), function( x ){
            var elem_this =  $(this);
            var src = elem_this.attr('data-src');
            elem_this.addClass('ele-' + x);    
            if( !elem_this.attr('data-status') ){
                var img = new Image();
                img.onload = function(){
                    $('#see-my-portfolio .ele-' + x + ' img').attr('src', src );
                    elem_this.attr('data-status', true);
                }
                img.src = src;
            }
        });
    }
});

function sweetalert(type, title, text){
    swal({ title: title, text: text, type: type });
}

$('#site-contact-us').on('submit', function(){
    var url, form, name, email, phone, message;
    id = $(this).attr('id');
    url = $(this).attr('action');
    form = $(this).serializeArray();

    name = $('#site-contact-us input[name=name]').val();
    email = $('#site-contact-us input[name=email]').val();
    phone = $('#site-contact-us input[name=phone]').val();
    message = $('#site-contact-us textarea[name=message]').val();

    if( !name || !email || !phone || !message ){
        swal({ type: 'error', title: 'Error', text: 'All the fields are required.', showConfirmButton: true });
    } else if( !/^[a-zA-Z ]+$/.test(name) ){
        swal({ type: 'error', title: 'Error', text: 'The name field contains invalid characters.', showConfirmButton: true });
    } else if( name.length < 2 ){
        swal({ type: 'error', title: 'Error', text: 'The name field can have minimum of 2 characters.', showConfirmButton: true });
    } else if( name.length > 55 ){
        swal({ type: 'error', title: 'Error', text: 'The name field can have maximum of 55 characters.', showConfirmButton: true });
    } else if( !/\S+@\S+\.\S+/.test(email) ){
        swal({ type: 'error', title: 'Error', text: 'The email field contains invalid email format.', showConfirmButton: true });
    } else if( email.length < 2 ){
        swal({ type: 'error', title: 'Error', text: 'The email field can have minimum of 5 characters.', showConfirmButton: true });
    } else if( !/^[0-9()-+ ]+$/.test(phone) ){
        swal({ type: 'error', title: 'Error', text: 'The phone field contains invalid phone format.', showConfirmButton: true });
    } else if( phone.length < 7 ){
        swal({ type: 'error', title: 'Error', text: 'The phone field can have minimum of 7 digits.', showConfirmButton: true });
    } else if( phone.length > 17 ){
        swal({ type: 'error', title: 'Error', text: 'The phone field can have maximum of 17 digits.', showConfirmButton: true });
    } else if( message.length < 5 ){
        swal({ type: 'error', title: 'Error', text: 'The message field can have minimum of 5 characters.', showConfirmButton: true });
    } else if( message.length > 255 ){
        swal({ type: 'error', title: 'Error', text: 'The message field can have maximum of 255 characters.', showConfirmButton: true });
    } else {
        swal({ type: 'info', title: 'Sending...', text: 'Please wait while your message is being sent.', showConfirmButton: false });
        $.post(url, form, function( data ){
            data = data.split('|');
            code = data[0];
            msg = data[1];
            if( code == 1 ){
                $('#site-contact-us')[0].reset();
                sweetalert('success', 'Good job!', msg);
            } else {
                sweetalert('error', 'An error occurred', msg);
            }
        });    
    }
    return false;
});

$(window).bind("load", function(){
    $('body').removeAttr('style');
});
