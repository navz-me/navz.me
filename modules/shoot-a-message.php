<div class="container">
    <h2 class="text-center heading orbitron">SHOOT A MESSAGE</h2>
    <form action="./contact.php" method="post" id="site-contact-us">
        <div class="row">
            <div class="col-left">
                <div class="field"><input type="text" name="name" placeholder="What's your name?"/></div>
                <div class="field"><input type="text" name="email" placeholder="What's your email address?"/></div>
                <div class="field"><input type="text" name="phone" placeholder="What's your phone number?"/></div>
            </div>
            <div class="col-right">
                <textarea name="message" id="" cols="30" rows="9" placeholder="What would you like to say to me?"></textarea>
            </div>
        </div>
        <div class="row"><button type="submit">Send Message</button></div>
    </form>
</div>