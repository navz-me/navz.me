<div class="container">
    <h2 class="text-center heading orbitron">PORTFOLIO</h2>
</div>
<div class="container">
    <h2 class="text-center">
        <ul class="filter">
            <li><a href="#" title="Filter by websites" class="current" data-filter="website"><i class="fa fa-filter" aria-hidden="true"></i> Website</a></li>
            <li><a href="#" title="Filter by mobiles apps" data-filter="mobile"><i class="fa fa-filter" aria-hidden="true"></i> Mobile</a></li>
            <li><a href="#" title="Filter by desktop apps" data-filter="desktop"><i class="fa fa-filter" aria-hidden="true"></i> Desktop</a></li>
            <li><a href="#" title="Filter by open source projects" data-filter="open-source"><i class="fa fa-filter" aria-hidden="true"></i> Open Source</a></li>
        </ul>
    </h2>
</div>
<div class="row grid">

    <?php 
        $default_src = "./images/navz-default-loading-placeholder.jpg"; 
        $lowdp_src = "./images/navz-default-loading-placeholder-456.jpg"; 
        $screenshots = [
            ['website', 'https://i.imgur.com/3TRVq5i.jpg', 'https://www.fijifor7s.com'],
            ['website', 'https://i.imgur.com/DWLrGmJ.jpg', 'http://www.natadolabeach.com.fj'],
            ['website', 'https://i.imgur.com/wu6Rlj3.jpg', 'http://www.pfip.org'],
            ['website', 'https://i.imgur.com/6MySVK3.jpg', 'http://www.piango.org'],
            ['website', 'https://i.imgur.com/Lukl7j9.jpg', 'http://phama.com.au'],
            ['website', 'https://i.imgur.com/rq4g6j9.jpg', 'http://www.bhikhabhai.com.fj'],
            ['website', 'https://i.imgur.com/8SMcPfi.jpg', 'http://www.nfitfiji.com/'],
            ['website', 'https://i.imgur.com/yGPBsTI.jpg', 'http://www.dominionfinance.com.fj'],
            ['website', 'https://i.imgur.com/gsp32AW.jpg', 'http://pafpnet.spc.int'],
            ['website', 'https://i.imgur.com/IVSJodr.jpg', 'http://www.fcdpfiji.org'],
            ['website', 'https://i.imgur.com/DzF9H0u.jpg', 'http://www.pacificagenciesfiji.com'],
            ['website', 'https://i.imgur.com/Obv5hEw.jpg', 'http://www.organicpasifika.com/poetcom/'],
            ['website', 'https://i.imgur.com/7ARj7Za.jpg', 'http://www.plp.org.fj'],
            ['website', 'https://i.imgur.com/582gC9N.jpg', 'http://www.fea.com.fj'],
            ['website', 'https://i.imgur.com/SWAGtqd.jpg', 'http://www.4fj.org.fj'],
            ['website', 'https://i.imgur.com/SMRdEJa.jpg', 'http://www.forumsec.org/'],
            ['website', 'https://i.imgur.com/N7tmWjo.jpg', 'https://picpa.usp.ac.fj'],
            ['website', 'https://i.imgur.com/yIg53Lr.jpg', 'http://pacificwomen.org/'],
            ['website', 'https://i.imgur.com/SjNsiBp.jpg', 'http://www.punjasflour.com/'],

            ['open-source', 'https://i.imgur.com/kJizr9c.jpg', 'https://wordpress.org/plugins/acf-page-grandchildren/'],
            ['open-source', 'https://i.imgur.com/6N0upUy.jpg', 'https://wordpress.org/plugins/acf-page-level/'],
            ['open-source', 'https://i.imgur.com/DyfUkd7.jpg', 'https://wordpress.org/plugins/wp-pages-advanced/'],
            ['open-source', 'https://i.imgur.com/qOJZYYG.jpg', 'https://wordpress.org/plugins/acf-token-input/'],
            ['open-source', 'https://i.imgur.com/YN8Jih2.jpg', 'https://wordpress.org/plugins/navz-photo-gallery/'],
            ['open-source', 'https://i.imgur.com/C4nTCQF.jpg', 'https://wordpress.org/plugins/wp-bootscraper/'],
    
        ];
    ?>
    <?php foreach($screenshots as $item ){ ?>
    <div class="column <?php echo $item[0]; ?>" data-src="<?php echo $item[1]; ?>">
        <a href="<?php echo $item[2]; ?>" target="__blank"><img class="loading-init" src="<?php echo $default_src; ?>"/></a>
    </div>
    <?php } ?>
</div>