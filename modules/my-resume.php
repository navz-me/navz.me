<div class="container">
    <h2 class="text-center heading orbitron">MY RESUME</h2>
    <div class="row">
        <div class="col column">
            <div class="icon"><i class="fa fa-tasks"></i></div>
            <div class="content">
                <h3>Oceanic Communications</h3>
                <div>Senior Application Developer</div>
                <div>07th August, 2012 - 16th April, 2018</div>
            </div>
        </div>
        <div class="col column">
            <div class="icon"><i class="fa fa-tasks"></i></div>
            <div class="content">
                <h3>Unitech Marketing (Fiji) Limited</h3>
                <div>I.T. Technician</div>
                <div>2011 - 2012</div>
            </div>
        </div>
        <div class="col column">
            <div class="icon"><i class="fa fa-tasks"></i></div>
            <div class="content">
                <h3>South Pacific Recording</h3>
                <div>Sales Person</div>
                <div>2009 - 2011</div>
            </div>
        </div>
        <div class="col column">
            <div class="icon"><i class="fa fa-tasks"></i></div>
            <div class="content">
                <h3>Budget Computers</h3>
                <div>Shop Assistant</div>
                <div>2009</div>
            </div>
        </div>
        <div class="col column">
            <div class="icon"><i class="fa fa-tasks"></i></div>
            <div class="content">
                <h3>Institute of Technology Australia</h3>
                <div>Student</div>
                <div>2008 - 2009</div>
            </div>
        </div>
        <div class="col column">
            <div class="icon"><i class="fa fa-tasks"></i></div>
            <div class="content">
                <h3>Tavua College</h3>
                <div>Student</div>
                <div>2004 - 2007</div>
            </div>
        </div>
        <div class="col column">
            <div class="icon"><i class="fa fa-tasks"></i></div>
            <div class="content">
                <h3>Tavua Primary</h3>
                <div>Student</div>
                <div>1996 - 2003</div>
            </div>
        </div>
    </div>
</div>