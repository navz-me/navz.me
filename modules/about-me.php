<div class="container">
    <h2 class="text-center heading orbitron">ABOUT ME</h2>
    <div class="excerpt">
        <p>Utilizing my 8+ years of self-taught skills in technology for a reputed company on basis of extensive web, mobile, servers and other platform development. I also have knowledge of Cloud Computing, solution architecture and networking.</p>
        <p>Knowledge in client/server web-technologies; portal and web-site engines, payment gateways; OOP in PHP5, OOP in JavaScript, DOM, AJAX, object oriented design patterns; Database connectivity, Database architecture, MySQL engines, XML-technologies, JSON, 3-rd party API integration and Database Management Systems, Enterprise Application Integration.</p>
        <p>I happily contribute my knowledge in Stackoverflow and WordPress forum. I own more than 5 open source projects.</p>
    </div>
</div>