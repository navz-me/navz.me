<div class="container">
    <ul class="justified">
        <li class="home"><a class="orbitron" href="#home">Home</a></li>
        <li class="about-me"><a class="orbitron" href="#about-me">About Me</a></li>
        <li class="my-resume"><a class="orbitron" href="#my-resume">My Resume</a></li>
        <li class="my-weapons"><a class="orbitron" href="#my-weapons">My Weapons</a></li>
        <li class="see-my-portfolio"><a class="orbitron" href="#see-my-portfolio">See My Portfolio</a></li>
        <li class="shoot-a-message"><a class="orbitron" href="#shoot-a-message">Contact Me</a></li>
    </ul>
</div>

<div id="mobo">
    <ul>
        <li class="home"><a class="orbitron" href="#home">Home</a></li>
        <li class="about-me"><a class="orbitron" href="#about-me">About Me</a></li>
        <li class="my-resume"><a class="orbitron" href="#my-resume">My Resume</a></li>
        <li class="my-weapons"><a class="orbitron" href="#my-weapons">My Weapons</a></li>
        <li class="see-my-portfolio"><a class="orbitron" href="#see-my-portfolio">See My Portfolio</a></li>
        <li class="shoot-a-message"><a class="orbitron" href="#shoot-a-message">Contact Me</a></li>
    </ul>
</div>