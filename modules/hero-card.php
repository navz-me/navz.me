<div class="container">

    <div class="image text-center"><img class="circle" src="./images/navneil.naicker-full-stack-developer.png"/></div>

    <div class="name">

        <h1 class="text-center orbitron">Navneil Naicker</h1>

    </div>

    <div class="tagline text-center">Full-Stack Software Developer and Open Source Contributor</div>

    <div class="social-icons text-center">

        <a target="_blank" href="https://github.com/navneil-naicker" class="fa fa-github" title="Github"></a>

        <a target="_blank" href="https://stackoverflow.com/users/3047846/navneil-naicker" class="fa fa-stack-overflow" title="Stackoverflow"></a>

        <a target="_blank" href="https://profiles.wordpress.org/navzme" class="fa fa-wordpress" title="WordPress"></a>

        <a target="_blank" href="https://www.linkedin.com/in/navneil/" class="fa fa-linkedin" title="Linkedin"></a>

        <a target="_blank" href="https://www.facebook.com/navz.me" class="fa fa-facebook" title="Facebook"></a>

        <a target="_blank" href="https://twitter.com/navzme" class="fa fa-twitter" title="Twitter"></a>

    </div>
    <?php /*
    <div class="download-resume">
        <a href="./navneil-naicker-curriculum-vitae.pdf" target="_blank"><i class="fa fa-cloud-download" aria-hidden="true" title="Click to download resume"></i> Download Resume</a>
    </div>
    */ ?>
</div>