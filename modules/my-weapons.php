<div class="container">
    <h2 class="text-center heading orbitron">MY WEAPONS</h2>
    <p class="excerpt">The following are self-taught technologies I work, familiar or have sound knowledge with but not limited to. <br/> I am always on the outlook for new challenges that adds value to my stack.</p>
    <div class="row languages">
        <div class="col-left">Languages: </div>
        <div class="col-right"><?php $languages = ['PHP', 'JavaScript', 'Ruby on Rails', 'Python', 'C#', 'VB.Net', 'Java', 'ColdFusion', ]; foreach($languages as $l ){ echo '<span class="tags">' . $l . '</span>'; } ?></div>
    </div>
    <div class="row database">
        <div class="col-left">Database: </div>
        <div class="col-right"><?php $database = ['MySQL', 'Maria', 'SQLITE']; foreach($database as $d ){ echo '<span class="tags">' . $d . '</span>'; } ?></div>
    </div>
     <div class="row markup">
        <div class="col-left">Markup: </div>
        <div class="col-right"><?php $markup = ['HTML', 'CSS', 'XML']; foreach($markup as $m ){ echo '<span class="tags">' . $m . '</span>'; } ?></div>
    </div>
    <div class="row sdk">
        <div class="col-left">SDK: </div>
        <div class="col-right"><?php $sdk = ['Facebook', 'Twitter', 'Google', 'YouTube']; foreach($sdk as $s ){ echo '<span class="tags">' . $s . '</span>'; } ?></div>
    </div>
    <div class="row framework">
        <div class="col-left">Framework: </div>
        <div class="col-right"><?php $framework = ['jQuery', 'Ionic', 'React/Native', 'Cordova', 'Angular', 'Vue.js', 'Node.js', 'Laravel', 'CodeIgniter', 'Bootstrap', 'Skeleton']; foreach($framework as $f ){ echo '<span class="tags">' . $f . '</span>'; } ?></div>
    </div>
    <div class="row platform">
        <div class="col-left">Platform: </div>
        <div class="col-right"><?php $platform = ['Web', 'Android', 'iOS', 'Windows']; foreach($platform as $p ){ echo '<span class="tags">' . $p . '</span>'; } ?></div>
    </div>
    <div class="row servers">
        <div class="col-left">Servers: </div>
        <div class="col-right"><?php $servers = ['Windows ', 'Debian distros']; foreach($servers as $sr ){ echo '<span class="tags">' . $sr . '</span>'; } ?></div>
    </div>
    <div class="row cms">
        <div class="col-left">CMS: </div>   
        <div class="col-right"><?php $cms = ['WordPress', 'Joomla', 'Drupal', 'Concreate5', 'Umbraco', 'Django', 'Magento']; foreach($cms as $c ){ echo '<span class="tags">' . $c . '</span>'; } ?></div>
    </div>
    <div class="row graphics">
        <div class="col-left">Graphics: </div>   
        <div class="col-right"><?php $graphics = ['Fireworks', 'Photoshop', 'Illustrator']; foreach($graphics as $g ){ echo '<span class="tags">' . $g . '</span>'; } ?></div>
    </div>
    <div class="row tools">
        <div class="col-left">Tools: </div>   
        <div class="col-right"><?php $tools = ['SASS', 'Gulp', 'Grunt', 'Git', 'SVN', 'VSC', 'Atom', 'PHPStorm', 'Nodepad++', 'Putty']; foreach($tools as $t ){ echo '<span class="tags">' . $t . '</span>'; } ?></div>
    </div>
</div>
